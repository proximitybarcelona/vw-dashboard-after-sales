<?php
	require_once("config.php");
	
	$message = "";
	
	if(isset($_REQUEST["u"]) && $_REQUEST["u"] != "" && isset($_REQUEST["p"]) && $_REQUEST["p"] != ""){
		$user = $_REQUEST["u"];
		$pass = md5($_REQUEST["p"]);
		
		$mysqli = new mysqli($db["host"], $db["user"], $db["pass"], $db["database"]);
		if ($mysqli->connect_errno) {}
		
		// Buscamos si existe el usuario con user y pass dado
		$query = "SELECT * FROM users WHERE usuario = '" + $user + "' and password = '" + $pass + "'";
		$resultado = $mysqli->query($query);
		
		//Si existe lo llevamos al Dashboard. Sino, mostramos mensaje error
		if(count($resultado) > 0){
			
			session_start();
			$_SESSION['logged'] = 1;
			
			header("Location: dashboard.php");
			die();
			
		}else{
			session_start();
			$_SESSION['logged'] = 0;
			
			$message = "Datos incorrectos";
		}
	}else{
		session_start();
		$_SESSION['logged'] = 0;
	}
	
	//echo md5("V0lks4g3N");
?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="es" > <![endif]-->
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Dashboard :: Volkswagen Posventa</title>

		<link rel="stylesheet" type="text/css" href="css/libs.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/main.css"/>

	</head>
	<body>
		<header class="row header">
			<img class="logo small-1 columns" src="img/logo-vw.jpg" alt="Volkswagen" />
			<h1 class="small-11 columns">VW Service Main Dashboard</h1>
		</header>
		
		<section class="row login">
			<div class="medium-4 medium-centered columns">
				<div class="large-12 columns">
					<p><?=$message?></p>
					<form method="post">
						<div class="row">
							<div class="large-12 columns">
								<input type="text" name="u" placeholder="Usuario" />
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<input type="password" name="p" placeholder="Contraseña" />
							</div>
						</div>
						<div class="row">
							<div class="large-12 large-centered columns">
								<input type="submit" class="button expand" value="Entrar"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
		
		<script src="js/libs.min.js"></script>
		<script src="js/main.js"></script>
		
	</body>
</html>