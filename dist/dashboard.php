<?php
	session_start();
	if (!isset($_SESSION['logged']) || $_SESSION['logged'] != 1) {
		header("Location: index.php");
		die();
	}
	
	require_once("config.php");
	
	$data = array();
	
	$mysqli = new mysqli($db["host"], $db["user"], $db["pass"], $db["database"]);
	if ($mysqli->connect_errno) {
	    echo "Fallo al conectar a MySQL: " . $mysqli->connect_errno;
	}
	
	// Recuperamos todos los datos de la BBDD y lo guardamos en un Array por ID_KPI
	$query = 'SELECT * FROM BASE_WEB_DATASCOUT ORDER BY ID_KPI';
	$resultado = $mysqli->query($query);
	while ($fila = $resultado->fetch_assoc()) {
		$data[$fila["ID_KPI"]] = $fila; 
	}
?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="es" > <![endif]-->
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Dashboard :: Volkswagen Posventa</title>

		<link rel="stylesheet" type="text/css" href="css/libs.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/main.css"/>
		
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		
		<script type="text/javascript">

			// Load the Visualization API and the piechart package.
			google.load('visualization', '1.0', {'packages':['corechart']});
			
			// Set a callback to run when the Google Visualization API is loaded.
			google.setOnLoadCallback(drawCharts);
			
			// Callback that creates and populates a data table
			function drawCharts() {
				
				var dataOutbounds = google.visualization.arrayToDataTable([
					['Channel', 'Private', 'Corporate', 'Other', { role: 'annotation' } ],
					['E-mail', <?=$data[3]["KPI_VALUE"]?>, <?=$data[6]["KPI_VALUE"]?>, <?=$data[85]["KPI_VALUE"]?>,''],
					['Mail', <?=$data[4]["KPI_VALUE"]?>, <?=$data[7]["KPI_VALUE"]?>,  <?=$data[86]["KPI_VALUE"]?>,''],
					['SMS', <?=$data[5]["KPI_VALUE"]?>, <?=$data[8]["KPI_VALUE"]?>,  <?=$data[87]["KPI_VALUE"]?>,'']
				]);
				
				var dataInbounds = google.visualization.arrayToDataTable([
					['Channel', 'Private', 'Corporate', 'Other', { role: 'annotation' } ],
					['Bono', <?=$data[33]["KPI_VALUE"]?>, <?=$data[35]["KPI_VALUE"]?>,  <?=$data[96]["KPI_VALUE"]?>,''],
					['Online Check', <?=$data[34]["KPI_VALUE"]?>, <?=$data[36]["KPI_VALUE"]?>,  <?=$data[97]["KPI_VALUE"]?>,'']
				]);
				
				var dataTmk = google.visualization.arrayToDataTable([
					['Channel', 'Kind of channel', { role: 'annotation' } ],
					['Private', <?=$data[61]["KPI_VALUE"]?>, ''],
					['Corporate', <?=$data[62]["KPI_VALUE"]?>, ''],
					['Other', <?=$data[106]["KPI_VALUE"]?>, '']
				]);

				outboundsChart(dataOutbounds);
				inboundsChart(dataInbounds);
				telemarketingChart(dataTmk);
				
			}

		</script>


	</head>
	<body>
		
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				
				<!-- Hamburger icon -->
				<!--nav>
					<a class="right-off-canvas-toggle" href="#" >
						<div class="menu">
							<div class="top-bar"></div>
							<div class="middle-bar"></div>
							<div class="bottom-bar"></div>
						</div>
	    			</a>
				</nav-->

				<!-- Off Canvas Menu -->
				<!--aside class="right-off-canvas-menu">
					<ul class="side-nav main">
						<li><a class="is-active" href="#">After sales plan result</a></li>
						<li><a href="#">Store results</a></li>
	        		</ul>
				</aside-->
				
				
				<!-- main content goes here -->
				<header class="row header">
					<img class="logo small-1 columns" src="img/logo-vw.jpg" alt="Volkswagen" />
					<h1 class="small-11 columns">VW Service Main Dashboard</h1>
				</header>
				
				<section class="row report">
					<header class="row">
						<h2 class="small-12 columns">After Sales Plan Result <a href="http://dashboard.proximitydata.es/QvAJAXZfc/opendoc.htm?document=Volkswagen%2FVolkswagen%2FVW%20After%20Sales.qvw&host=QVS%40qlikview" target="_blank"><span class="fa flaticon-externallink"></span></a></h2>
					</header>
			
					<section class="row">
						
						<div class="small-12 columns">
						
							<!-- Outbounds -->
							
							<!-- Caja 1 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[1]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[1]["KPI_VALUE"]?>">0</span>
										<span class="kpi-comparative">LY: <?=number_format($data[2]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[1]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-email fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[1]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[1]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[1]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div id="outbound_chart"></div>
								</div>
							</div>
							
							<!-- Caja 2 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[9]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[9]["KPI_VALUE"]?>">0&euro;</span>
										<span class="kpi-comparative">LY: <?=number_format($data[10]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[9]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-funnel fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[9]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[9]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[9]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
									<div class="small-12 columns">
										<span class="kpi-rate"><?=$data[115]["KPI_NAME"]?>: <?=number_format($data[115]["KPI_VALUE"], 0, ',', '.')?>%</span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[12]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[12]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[13]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[14]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[15]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[16]["KPI_NAME"]?></h4>
										<div class="small-7 columns">
											<span class="kpi-secondary"><?=number_format($data[16]["KPI_VALUE"], 0, ',', '.')?></span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[17]["KPI_VALUE"], 0, ',', '.')?></span>
										</div>
										<div class="small-5 columns">
											<span class="kpi-secondary">(<?=number_format($data[18]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											<span class="kpi-comparative-secondary">(<?=number_format($data[19]["KPI_VALUE"], 0, ',', '.')?>%)</span>
										</div>
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[88]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[88]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[89]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[90]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[91]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Caja 3 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[20]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[20]["KPI_VALUE"]?>">0</span><span class="kpi-number">&euro;</span>
										<span class="kpi-comparative">LY: <?=number_format($data[21]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[20]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-incomes fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[20]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[20]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[20]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[23]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-12 columns">
												<span class="kpi-secondary"><?=number_format($data[23]["KPI_VALUE"], 0, ',', '.')?>&euro; <br> (<?=number_format($data[25]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[24]["KPI_VALUE"], 0, ',', '.')?>&euro;<br> (<?=number_format($data[26]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											</div>
											<!--div class="small-4 columns">
												<span class="kpi-secondary">(<?=$data[25]["KPI_VALUE"]?>&euro;)</span>
												<span class="kpi-comparative-secondary">(<?=$data[26]["KPI_VALUE"]?>&euro;)</span>
											</div-->
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[27]["KPI_NAME"]?></h4>
										<div class="small-12 columns">
											<span class="kpi-secondary"><?=number_format($data[27]["KPI_VALUE"], 0, ',', '.')?>&euro;<br>(<?=number_format($data[29]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[28]["KPI_VALUE"], 0, ',', '.')?>&euro;<br>(<?=number_format($data[30]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
										</div>
										<!--div class="small-4 columns">
											<span class="kpi-secondary">(<?=$data[29]["KPI_VALUE"]?>&euro;)</span>
											<span class="kpi-comparative-secondary">(<?=$data[30]["KPI_VALUE"]?>&euro;)</span>
										</div-->
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[92]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[92]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[93]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[94]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[95]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Inbounds -->
							
							<!-- Caja 4 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[31]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[31]["KPI_VALUE"]?>">0</span>
										<span class="kpi-comparative">LY: <?=number_format($data[32]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[31]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-form fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[31]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[31]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[31]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div id="inbound_chart"></div>
								</div>
							</div>
							
							<!-- Caja 5 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[37]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[37]["KPI_VALUE"]?>">0</span>
										<span class="kpi-comparative">LY: <?=number_format($data[38]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[37]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-funnel fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[37]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[37]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[37]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
									<div class="small-12 columns">
										<span class="kpi-rate"><?=$data[116]["KPI_NAME"]?>: <?=number_format($data[116]["KPI_VALUE"], 0, ',', '.')?>%</span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[40]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[40]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[41]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[42]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[43]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[44]["KPI_NAME"]?></h4>
										<div class="small-7 columns">
											<span class="kpi-secondary"><?=number_format($data[44]["KPI_VALUE"], 0, ',', '.')?></span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[45]["KPI_VALUE"], 0, ',', '.')?></span>
										</div>
										<div class="small-5 columns">
											<span class="kpi-secondary">(<?=number_format($data[46]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											<span class="kpi-comparative-secondary">(<?=number_format($data[47]["KPI_VALUE"], 0, ',', '.')?>%)</span>
										</div>
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[98]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[98]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[99]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[100]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[101]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Caja 6 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[48]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[48]["KPI_VALUE"]?>">0</span><span class="kpi-number">&euro;</span>
										<span class="kpi-comparative">LY: <?=number_format($data[49]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[48]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-incomes fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[48]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[48]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[48]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[51]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-8 columns">
												<span class="kpi-secondary"><?=number_format($data[51]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[52]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
											</div>
											<div class="small-4 columns">
												<span class="kpi-secondary">(<?=number_format($data[53]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[54]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											</div>
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[55]["KPI_NAME"]?></h4>
										<div class="small-8 columns">
											<span class="kpi-secondary"><?=number_format($data[55]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[56]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
										</div>
										<div class="small-4 columns">
											<span class="kpi-secondary">(<?=number_format($data[57]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											<span class="kpi-comparative-secondary">(<?=number_format($data[58]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
										</div>
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[102]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[102]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[103]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[104]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[105]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Telemarketing -->
							
							<!-- Caja 7 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[59]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[59]["KPI_VALUE"]?>">0</span>
										<span class="kpi-comparative">LY: <?=number_format($data[60]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[59]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-telemarketer fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[59]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[59]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[59]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div id="telemarketing_chart"></div>
								</div>
							</div>
							
							<!-- Caja 8 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[63]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[63]["KPI_VALUE"]?>">0</span>
										<span class="kpi-comparative">LY: <?=number_format($data[64]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[63]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-tmktrack fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[63]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[63]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[63]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
									<div class="small-12 columns">
										<span class="kpi-rate"><?=$data[117]["KPI_NAME"]?>: <?=number_format($data[117]["KPI_VALUE"], 0, ',', '.')?>%</span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[66]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[66]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[67]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[68]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[69]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[70]["KPI_NAME"]?></h4>
										<div class="small-7 columns">
											<span class="kpi-secondary"><?=number_format($data[70]["KPI_VALUE"], 0, ',', '.')?></span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[71]["KPI_VALUE"], 0, ',', '.')?></span>
										</div>
										<div class="small-5 columns">
											<span class="kpi-secondary">(<?=number_format($data[72]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											<span class="kpi-comparative-secondary">(<?=number_format($data[73]["KPI_VALUE"], 0, ',', '.')?>%)</span>
										</div>
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[107]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[107]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[108]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[109]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[110]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Caja 9 -->
							<div class="kpi small-12 medium-6 large-4 columns">
								<div class="front">
									<div class="data small-12 medium-8 columns">
										<h3><?=$data[74]["KPI_NAME"]?></h3>
										<span class="kpi-number" data-number="<?=$data[74]["KPI_VALUE"]?>">0</span><span class="kpi-number">&euro;</span>
										<span class="kpi-comparative">LY: <?=number_format($data[75]["KPI_VALUE"], 0, ',', '.')?></span>
									</div>
									<div class="chart <?=strtolower($data[74]["COLOR_OBJ"])?> small-12 medium-4 columns">
										<span class="icon fa flaticon-funnel fa-stack-2x fa-inverse"></span>
										<span class="piechart" data-percent="<?=$data[74]["PERCENT_OBJ"]?>" data-line-width="10" data-line-cap="butt" data-size="100" data-animate="2000" data-target=".kpi-number" data-bar-color="<?=$colors[strtolower($data[74]["COLOR_OBJ"])]["line"]?>" data-scale-color="false" data-track-color="<?=$colors[strtolower($data[74]["COLOR_OBJ"])]["background"]?>"></span>
									</div>
								</div>
								<div class="back">
									<div class="private small-12 medium-6 columns">
										<h4><?=$data[77]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[77]["KPI_VALUE"], 0, ',', '.')?></span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[78]["KPI_VALUE"], 0, ',', '.')?></span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[79]["KPI_VALUE"], 0, ',', '.')?>%)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[80]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											</div>
										</div>
									</div>
									<div class="company small-12 medium-6 columns">
										<h4><?=$data[81]["KPI_NAME"]?></h4>
										<div class="small-7 columns">
											<span class="kpi-secondary"><?=number_format($data[81]["KPI_VALUE"], 0, ',', '.')?></span>
											<span class="kpi-comparative-secondary">LY: <?=number_format($data[82]["KPI_VALUE"], 0, ',', '.')?></span>
										</div>
										<div class="small-5 columns">
											<span class="kpi-secondary">(<?=number_format($data[83]["KPI_VALUE"], 0, ',', '.')?>%)</span>
											<span class="kpi-comparative-secondary">(<?=number_format($data[84]["KPI_VALUE"], 0, ',', '.')?>%)</span>
										</div>
									</div>
									<div class="na small-12 columns">
										<h4><?=$data[111]["KPI_NAME"]?></h4>
										<div class="border-column">
											<div class="small-7 columns">
												<span class="kpi-secondary"><?=number_format($data[111]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
												<span class="kpi-comparative-secondary">LY: <?=number_format($data[112]["KPI_VALUE"], 0, ',', '.')?>&euro;</span>
											</div>
											<div class="small-5 columns">
												<span class="kpi-secondary">(<?=number_format($data[113]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
												<span class="kpi-comparative-secondary">(<?=number_format($data[114]["KPI_VALUE"], 0, ',', '.')?>&euro;)</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</section>
			
				</section>
				
				<!--section class="row report">
					<header class="row">
						<h2 class="small-12 columns">Store Result <a href="#"><span class="fa flaticon-externallink"></span></a></h2>
					</header>
			
					<section class="row">
						<p class="small-12 columns"></p>
					</section>
			
				</section-->
  
  
				<!-- close the off-canvas menu -->
				<a class="exit-off-canvas"></a>

			</div>
		</div>
		
		<script src="js/libs.min.js"></script>
		<script src="js/main.js"></script>
		
	</body>
</html>


<?php

			
	// Liberar resultados
	$resultado->free();
	
	// Cerrar la conexión
	$mysqli->close();

	
?>