$(document).foundation();

$(function() {

	$(".right-off-canvas-toggle, .exit-off-canvas").click(function() {
    	$(".top-bar").toggleClass("top-bar-close");
		$(".middle-bar").toggleClass("middle-bar-close");
		$(".bottom-bar").toggleClass("bottom-bar-close");
  	});
  	
  	$(".kpi").on( "click", function() {
		var $this = $(this);
		$this.hasClass("flipped") === true ? $this.removeClass("flipped") : $this.addClass("flipped");
    });
  	
  	$('.kpi .front .piechart').easyPieChart();
    
    var point_separator_number_step = $.animateNumber.numberStepFactories.separator('.');
    
    $.each($('.kpi .front .kpi-number'), function(){
	   
		$(this).animateNumber({
		    number: $(this).attr("data-number"),
		    numberStep: point_separator_number_step
		}, 2000);

	    
    });
});


var blue = "#064674";
var middleBlue = "#4087ba";
var lightBlue = "#8fbfe1";

function outboundsChart(data){
				
	var options = {
		title: "Outbound channel contact distribution",
		titleTextStyle: {fontSize: 12, bold: false},
		height: 180,
		legend: { position: 'bottom', maxLines: 1 },
		bar: { groupWidth: '75%' },
		colors: [blue, middleBlue, lightBlue],
		isStacked: true
	};
	
	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.BarChart(document.getElementById('outbound_chart'));
	chart.draw(data, options);
}

function inboundsChart(data){
	
	var options = {
		title: "Inbound channel contact distribution",
		titleTextStyle: {fontSize: 12, bold: false},
		height: 180,
		legend: { position: 'bottom', maxLines: 1 },
		bar: { groupWidth: '75%' },
		colors: [blue, middleBlue, lightBlue],
		isStacked: true
	};
	
	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.BarChart(document.getElementById('inbound_chart'));
	chart.draw(data, options);
}

function telemarketingChart(data) {
	
	var options = {
		title: 'Sent to TMK customer distribution',
		titleTextStyle: {fontSize: 12, bold: false},
		height: 180,
		colors: [blue, middleBlue, lightBlue],
		legend: { alignment: 'center'}
	};
	
	var chart = new google.visualization.PieChart(document.getElementById('telemarketing_chart'));
	chart.draw(data, options);
}

