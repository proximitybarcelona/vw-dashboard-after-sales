var gulp = require('gulp'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	minifycss = require('gulp-minify-css'),
	rename = require('gulp-rename'),
	gzip = require('gulp-gzip'),
	livereload = require('gulp-livereload'),
	concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant');

var gzip_options = {
    threshold: '1kb',
    gzipOptions: {
        level: 9
    }
};

/* Compile Sass */
gulp.task('sass-libs', function() {
    return gulp.src([
    		'scss/main.scss',
    		'bower_components/fontawesome/scss/font-awesome.scss'
    	])
        .pipe(sass())
        .pipe(concat('libs.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('dist/css'))
        .pipe(gzip(gzip_options))
        .pipe(gulp.dest('dist/css'))
        .pipe(livereload());
});

gulp.task('sass-main', function() {
    return gulp.src([
    		'scss/styles.scss'
    	])
        .pipe(sass())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('dist/css'))
        .pipe(gzip(gzip_options))
        .pipe(gulp.dest('dist/css'))
        .pipe(livereload());
});

/* Minify Js */
gulp.task('scripts', function(){
    return gulp.src([
    		'bower_components/jquery/dist/jquery.js',
    		'bower_components/modernizr/modernizr.js',
    		'bower_components/jquery.cookie/jquery.cookie.js',
    		'bower_components/jquery-placeholder/jquery.placeholder.js',
    		'bower_components/fastclick/lib/fastclick.js',
    		'bower_components/foundation/js/foundation.js',
    		//'bower_components/foundation/js/foundation/foundation.offcanvas.js',
			'bower_components/mobile-detect/mobile-detect.js',
    		//'bower_components/gsap/src/uncompressed/TweenMax.js',
    		//'bower_components/gsap/src/uncompressed/TimelineLite.js',
    		//'bower_components/Chart.js/Chart.js',
    		'bower_components/jquery-animateNumber/jquery.animateNumber.js',
    		'bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.js',
    		//'bower_components/Graphs/dist/jquery.Graphs.js',
			//'js/main.js'
    	])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(gzip(gzip_options))
        .pipe(gulp.dest('dist/js'))
        .pipe(livereload());
});

/* Images optimization */
gulp.task('images', function () {
    return gulp.src('images/**/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({
			optimizationLevel: 3,
			progressive: true,
			interlaced: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
        .pipe(gulp.dest('dist/img'));
});

/* Icons */

gulp.task('icons', function() {
    return gulp.src('bower_components/fontawesome/fonts/**.*')
        .pipe(gulp.dest('dist/fonts'));
});


/* Watch Files For Changes */
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('scss/*.scss', ['sass']);
    gulp.watch('js/*.js', ['scripts']);
	gulp.watch('images/**/*.{png,jpg,jpeg,gif,svg}', ['images']);

});

gulp.task('default', ['sass-libs', 'sass-main', 'scripts', 'images', 'icons', 'watch']);
